/**
 * <region-list>
 * -----------------------------------------------------------------------------
 * A list of regions
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('regionList', {
  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'regionsTitle',
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      regions: []
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
    <div class="region-list">
      <h2>{{ regionsTitle }}</h2>
      <div>
        <ul class="nav flex-column">
            <li class="nav-item" v-for="region in regions">
              <a class="nav-link" :href="buildUrl(region)" :target="buildTarget(region)">
                {{ region.name }}, {{ region.locality }}
              </a>
            </li>
        </ul>
      </div>
    </div>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  mounted: async function(){
    this.regions = await Cloud.activeRegions();
  },
  // ^Note that there is no `beforeDestroy()` lifecycle callback in this
  // component. This is on purpose, since the timing vs. `leave()` gets tricky.

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    buildUrl (region) {
      return region.externalUrl ? region.externalUrl : '/' + region.key;
    },
    buildTarget (region) {
      return region.externalUrl ? "_blank" : "_self";
    }
  }
});
