module.exports = {


  friendlyName: 'Format a region title.',


  description: 'Take a region and make it look like Milwaukee, WI or something.',


  inputs: {

    region: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

  },


  fn: function (inputs) {

    // All done.
    return Region.name + Region.locality ? ', ' + Region.locality : '';

  }


};
