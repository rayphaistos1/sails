module.exports = {


  friendlyName: 'Active regions',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    const regionParams = { sort: 'name' };
    regionParams.where = sails.config.custom.activeFilter;
    return await Region.find(
      regionParams
    );
  }


};

