module.exports = {


  friendlyName: 'Login page',


  description: 'Find the login page based on current region or go to the default',


  inputs: {
    req: {
      type: 'ref',
      required: true,
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    // get the region from the session
    let region = inputs.req.session.region;

    // if none, get the default region from the custom config
    if (!region) {
      region = sails.config.custom.defaultRegion;
    }

    return exits.success('/' + region + '/login');
  }


};
