module.exports = {


  friendlyName: 'Active payees',


  description: '',


  inputs: {

    region: {
      type: 'string',
      required: true,
    },

    tags: {
      type: 'string',
      required: false,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    const out = [];
    const payeeParams = { sort: 'name' };
    payeeParams.where = sails.config.custom.activeFilter;
    const payees = await Payee.find(payeeParams)
      .populate('region')
      .populate('tags');

    let tags = inputs.tags;
    if (tags) {
      tags = tags.split(',');
    }

    const hasTags = tags && tags.length > 0;

    payees.forEach( function (payee) {
      if (inputs.region === payee.region.key) {
        // skip tag test if there aren't any, otherwise default to false
        let tagMatched = !hasTags;
        if (hasTags && payee.tags.length > 0) {
          tags.forEach(function (filterTag) {
            payee.tags.forEach(function (payeeTag) {
              if (filterTag === payeeTag.name) {
                tagMatched = true;
              }
            });
          });
        }
        if (tagMatched) {
          out.push(payee);
        }
      }
    });

    return out;
  }


};

