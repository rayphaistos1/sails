module.exports = {


  friendlyName: 'Active tags',


  description: '',


  inputs: {

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    return await Tag.find(sails.config.custom.activeFilter);
  }


};

