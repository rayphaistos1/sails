/**
 * Payee.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    name: {
      type: 'string',
      required: true,
      description: 'Name of business or worker, could be a nickname'
    },

    address: {
      type: 'string',
      allowNull: true,
      example: '123 Fake St.',
    },

    contactPerson: {
      type: 'string',
      allowNull: true,
      example: 'Jo Schmoe',
    },

    contactEmail: {
      type: 'string',
      allowNull: true,
      example: 'Jo.Schmoe@example.com',
    },

    contactPhone: {
      type: 'string',
      allowNull: true,
      example: '414-555-1212',
    },

    description: {
      type: 'string',
      allowNull: true,
      example: 'A few words about the business.',
    },

    website: {
      type: 'string',
      allowNull: true,
      example: 'https://example.com',
    },

    parentName: {
      type: 'string',
      allowNull: true,
      description: 'Placeholder in case someone signs up as a person but the business does not have a record',
    },

    cashapp: {
      type: 'string',
      allowNull: true,
      example: '$CashAppName',
    },

    paypal: {
      type: 'string',
      allowNull: true,
      example: 'https://paypal.me/PayPalName',
    },

    venmo: {
      type: 'string',
      allowNull: true,
      example: '@VenmoName',
    },

    gofundme: {
      type: 'string',
      allowNull: true,
      example: 'https://www.gofundme.com/f/hospitality-industry-associates-fund',
    },

    zelle: {
      type: 'string',
      allowNull: true,
      example: 'email or phone number',
    },

    etsy: {
      type: 'string',
      allowNull: true,
      example: 'https://etsy.com/shop/MyEtsyShop',
    },

    bandcamp: {
      type: 'string',
      allowNull: true,
      example: 'https://MyBandName.bandcamp.com',
    },

    soundcloud: {
      type: 'string',
      allowNull: true,
      example: 'https://soundcloud.com/mysoundcloud',
    },

    facebook: {
      type: 'string',
      allowNull: true,
      example: 'https://facebook.com/shop/MyFacebook',
    },

    twitter: {
      type: 'string',
      allowNull: true,
      example: 'https://twitter.com/MyTwitter',
    },

    instagram: {
      type: 'string',
      allowNull: true,
      example: 'https://instagram.com/shop/myinstagram',
    },

    other: {
      type: 'string',
      allowNull: true,
      example: 'https://example.com/someOtherPage',
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    children: {
      collection: 'payee',
      via: 'parents',
    },

    parents: {
      collection: 'payee',
      via: 'children',
    },

    region: {
      model: 'region',
    },

    tags: {
      collection: 'tag',
      via: 'payees',
    },

  },

};
