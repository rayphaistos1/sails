module.exports = {


  friendlyName: 'Find one tag',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound'
    },

  },


  fn: async function (inputs) {

    // All done.
    const tag =  await Tag.findOne({id: inputs.id});
    if (tag) {
      return tag;
    }
    throw 'notFound';

  }


};
