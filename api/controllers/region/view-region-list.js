module.exports = {


  friendlyName: 'View region list',


  description: 'Display "Region list" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/region/region-list'
    }

  },


  fn: async function () {

    // Respond with view.
    return {
      regions: await sails.helpers.activeRegions(),
    };

  }


};
