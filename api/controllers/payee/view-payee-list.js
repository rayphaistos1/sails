module.exports = {


  friendlyName: 'View payee list',


  description: 'Display "Payee list" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/payee/payee-list'
    }

  },


  fn: async function () {

    const region = await sails.helpers.regionByKey(this.req.param('region'));

    // Respond with view.
    return { region };

  }


};
