module.exports = {


  friendlyName: 'View payee detail',


  description: 'Display "Payee detail" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/payee/payee-detail'
    }

  },


  fn: async function () {

    const region = await sails.helpers.regionByKey(this.req.param('region'));

    // Respond with view.
    return { region };

  }


};
