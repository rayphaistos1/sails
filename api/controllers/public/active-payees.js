module.exports = {


  friendlyName: 'List active payees',


  description: 'Active payee.',


  inputs: {

    region: {
      type: 'string',
      required: true,
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    return this.res.json(
      await sails.helpers.activePayees(
        inputs.region,
        this.req.param('tags')
      )
    );

  }


};
