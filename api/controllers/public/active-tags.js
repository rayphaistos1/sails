module.exports = {


  friendlyName: 'List active tags',


  description: 'Active tag.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    return this.res.json(await sails.helpers.activeTags());

  }


};
