module.exports = {


  friendlyName: 'List active regions',


  description: 'Active region.',


  inputs: {

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    return this.res.json(await sails.helpers.activeRegions());

  }


};
