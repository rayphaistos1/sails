module.exports = {


  friendlyName: 'Find one fund',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound'
    },

  },


  fn: async function (inputs) {

    // All done.
    const fund = await Fund.findOne({id: inputs.id});
    if (fund) {
      return fund;
    }
    throw 'notFound';

  }


};
