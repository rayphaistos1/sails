module.exports = {


  friendlyName: 'View suggest fund form',


  description: 'Display "Suggest fund form" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/fund/suggest-fund-form'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
