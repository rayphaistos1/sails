/**
 * Local environment settings
 *
 * Use this file to specify configuration settings for use while developing
 * the app on your personal system.
 *
 * For more information, check out:
 * https://sailsjs.com/docs/concepts/configuration/the-local-js-file
 */

module.exports = {

  // Any configuration settings may be overridden below, whether it's built-in Sails
  // options or custom configuration specifically for your app (e.g. Stripe, Mailgun, etc.)
  custom: {
    // this is the dev setting, replace with your real site for prod
    baseUrl: 'http://localhost:1337',

    // probably the region key of your home town (I use airport codes)
    defaultRegion: 'mke',

    // admin email address
    internalEmailAddress: 'where-to-send-notifications@example.com',

    // from address for site emails
    fromEmailAddress: 'noreply@example.com',

    // from name for site emails
    fromName: 'The Example.com Team',

    // get these settings from the mailgun API console
    mailgunDomain: 'https://api.mailgun.net/v3/sandbox12345.mailgun.org',
    mailgunSecret: '122234465fefc-432e60-997e341',
  },

  datastores: {
    default: {
      adapter: 'sails-mongo',

      // where to store the model objects
      url: 'mongodb://user:password@localhost:27017/tipjar',
    },
  },

  http: {
    trustProxy: true,
  },

  models: {
    dataEncryptionKeys: {
      // cut and paste from config/models.js so you don't check it into source control
      default: 'sdfsDSDffssw+acbhjud76677s++cads45='
    },
  },

  session: {
    adapter: 'connect-mongo',
    cookie: {
      secure: true,
    },

    // cut and paste from config/session.js so you don't check it into source control
    secret: '8786678ae808989cbd988080c5',

    // use a different database for session storage or else use redis
    url: 'mongodb://user:password@localhost:27017/tipjarSession',
  },

  sockets: {
    onlyAllowOrigins: [
      // this is the dev setting, replace with your real site for prod
      'http://localhost:1337',
    ],
  },

};
